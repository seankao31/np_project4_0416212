#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <unistd.h>
#include <errno.h>
#include <netdb.h>
#include <fcntl.h>
#include <iostream>
#include <string>
#include <vector>
#include <utility>

#include "utility.h"

using namespace std;

#define NOTCONNECT 0
#define NOTWRITABLE 1
#define WRITABLE 2
#define EXIT 3
#define WAITSOCKSERVER 4

struct client {
    string host, port, file;
    string sockhost, sockport;
    int id;
    int sock, socksock;
    int status;
    struct sockaddr_in sin, socksin;
    FILE *fin;
    bool valid;
};

void errorlog(string);
void htmloutput(string, int, bool is_command = false);
void html_error_output(string);

vector<pair<string, string> > parse_query(string);
void html_init_output(vector<struct client>);

int main() {
    string query = getenv("QUERY_STRING");
    // cout << query << endl;
    // string query = "h1=nplinux3.cs.nctu.edu.tw&p1=2345&f1=t1.txt&h2=&p2=&f2=&h3=&p3=&f3=&h4=&p4=&f4=&h5=&p5=&f5=";
    auto parsed_query = parse_query(query);

    cerr << "CONTENT_LENGTH: " << getenv("CONTENT_LENGTH") << endl;
    cerr << "REQUEST_METHOD: " << getenv("REQUEST_METHOD") << endl;
    cerr << "SCRIPT_NAME: " << getenv("SCRIPT_NAME") << endl;
    cerr << "REMOTE_HOST: " << getenv("REMOTE_HOST") << endl;
    cerr << "REMOTE_ADDR: " << getenv("REMOTE_ADDR") << endl;
    cerr << "AUTH_TYPE: " << getenv("AUTH_TYPE") << endl;
    cerr << "REMOTE_USER: " << getenv("REMOTE_USER") << endl;
    cerr << "REMOTE_IDENT: " << getenv("REMOTE_IDENT") << endl;

    vector<struct client> clients;
    fd_set rfds, afds;
    int nfds = getdtablesize();
    if (FD_SETSIZE < nfds)
        nfds = FD_SETSIZE;
    FD_ZERO(&afds);

    // parse query
    int id = 0;
    for (int i = 0; i < 5; ++i) {
        struct client cli;
        cli.host = parsed_query[i*5].second;
        cli.port = parsed_query[i*5+1].second;
        cli.file = parsed_query[i*5+2].second;
        cli.sockhost = parsed_query[i*5+3].second;
        cli.sockport = parsed_query[i*5+4].second;
        if (!cli.host.empty() && !cli.port.empty() && !cli.file.empty() && !cli.sockhost.empty() && !cli.sockport.empty()) {
            cli.valid = true;
        }
        else {
            cli.valid = false;
        }
        cli.id = id++;
        clients.push_back(cli);
    }

    // add clients
    for (auto &cli: clients) {
        if (!cli.valid) {
            continue;
        }
        struct hostent *phe;

        bzero((char*)&cli.socksin, sizeof(cli.socksin));
        cli.socksin.sin_family = AF_INET;
        cli.socksin.sin_port = htons((u_short)atoi(cli.sockport.c_str()));
        if ((phe = gethostbyname(cli.sockhost.c_str())))
            cli.socksin.sin_addr = *((struct in_addr *) phe->h_addr_list[0]);
        else
            cli.socksin.sin_addr.s_addr = inet_addr(cli.sockhost.c_str());
        cli.socksock = socket(PF_INET, SOCK_STREAM, 0);
        // int flags = fcntl(cli.socksock, F_GETFL, 0);
        // fcntl(cli.socksock, F_SETFL, flags | O_NONBLOCK);

        bzero((char*)&cli.sin, sizeof(cli.sin));
        cli.sin.sin_family = AF_INET;
        cli.sin.sin_port = htons((u_short)atoi(cli.port.c_str()));
        if ((phe = gethostbyname(cli.host.c_str())))
            cli.sin.sin_addr = *((struct in_addr *) phe->h_addr_list[0]);
        else
            cli.sin.sin_addr.s_addr = inet_addr(cli.host.c_str());
        cli.fin = fopen(cli.file.c_str(), "r");
        FD_SET(fileno(cli.fin), &afds);
        cli.status = NOTCONNECT;
        // TODO: sleep??
        // usleep(100000);
    }

    html_init_output(clients);
    for (auto cli: clients) {
        if (cli.host.empty()) {
            htmloutput("Server is not exist\n", cli.id, false);
        }
        if (cli.port.empty()) {
            htmloutput("Port is not exist\n", cli.id, false);
        }
        if (cli.file.empty()) {
            htmloutput("File is not exist\n", cli.id, false);
        }
    }

    // try connect
    for (int i = 0; i < clients.size(); ++i) {
        struct client &cli = clients[i];
        if (!cli.valid) {
            continue;
        }
        errorlog("try connect " + to_string(i));
        if (connect(cli.socksock, (struct sockaddr *) &cli.socksin, sizeof(cli.socksin)) < 0) {
            if (errno != EINPROGRESS && errno != EALREADY) {
                errorlog("Connect Failed:");
                errorlog(strerror(errno));
                return -1;
            }
        }
        // BUG: even if connect not success, it will consider it success
        else {
            errorlog("Connect Success");
            FD_SET(cli.socksock, &afds);
            cli.status = WAITSOCKSERVER;
            unsigned char request[9] = {0};
            request[0] = 4;
            request[1] = 1;
            request[2] = ((u_short)atoi(cli.port.c_str()) >> 8);
            request[3] = ((u_short)atoi(cli.port.c_str()) & 0xFF);
            string ip = inet_ntoa(cli.sin.sin_addr);
            vector<string> ips = split(ip, ".");
            request[4] = stoi(ips[0]);
            request[5] = stoi(ips[1]);
            request[6] = stoi(ips[2]);
            request[7] = stoi(ips[3]);
            request[8] = 0;
            write(cli.socksock, request, 9);
        }
    }

    int exit = 0;

    for (auto cli: clients) {
        int flags = fcntl(cli.socksock, F_GETFL, 0);
        fcntl(cli.socksock, F_SETFL, flags | O_NONBLOCK);
    }

    while(true) {
        int num_valid = 0;
        for (auto cli: clients) {
            if (cli.valid)
                num_valid++;
        }
        if (exit == num_valid)
            break;

        memcpy(&rfds, &afds, sizeof(rfds));
        if (select(nfds, &rfds, (fd_set *)0, (fd_set *)0, (struct timeval *)0) < 0) {
            errorlog("Select Failed:");
            errorlog(strerror(errno));
        }

        // for (int i = 0; i < clients.size(); ++i) {
        //     struct client &cli = clients[i];
        for (auto &cli: clients) {
            if (!cli.valid)
                continue;
            if (cli.status == EXIT)
                continue;
            // errorlog("FD_ISSET (id: " + to_string(cli.id) + ", sock: " + to_string(cli.sock) + "): " + to_string(FD_ISSET(cli.sock, &rfds)));

            // myconnect
            int error = 0;
            socklen_t n = sizeof(error);
            if (cli.status == NOTCONNECT) {
                // errorlog("try connect " + to_string(i));
                errorlog("try connect " + to_string(cli.id));
                if (getsockopt(cli.socksock, SOL_SOCKET, SO_ERROR, &error, &n) < 0 || error != 0) {
                    // non-blocking connect failed
                    errorlog("Non Blocking Connect Failed");
                    return -1;
                }
                // BUG: even if connect not success, it will consider it success
                errorlog("Connect Success");
                FD_SET(cli.socksock, &afds);
                cli.status = WAITSOCKSERVER;
                unsigned char request[9] = {0};
                request[0] = 4;
                request[1] = 1;
                request[2] = ((u_short)atoi(cli.port.c_str()) >> 8);
                request[3] = ((u_short)atoi(cli.port.c_str()) & 0xFF);
                string ip = inet_ntoa(cli.sin.sin_addr);
                vector<string> ips = split(ip, ".");
                request[4] = stoi(ips[0]);
                request[5] = stoi(ips[1]);
                request[6] = stoi(ips[2]);
                request[7] = stoi(ips[3]);
                request[8] = 0;
                write(cli.socksock, request, 9);
            }

            if (cli.status == WAITSOCKSERVER) {
                unsigned char readbuf[8];
                int len;
                if ((len = ureadn(cli.socksock, readbuf, 8)) < 0) {
                    cerr << "Read Error" << endl;
                }
                else if (len == 0) {
                }
                else {
                    if (int(readbuf[1]) == 90) {
                        cli.status = NOTWRITABLE;
                    }
                    else {
                        FD_CLR(cli.socksock, &afds);
                        close(cli.socksock);
                        continue;
                    }
                }
            }

            if (cli.status == WRITABLE && FD_ISSET(fileno(cli.fin), &rfds)) {
                char cmdbuff[MAXLINE];
                if (fgets(cmdbuff, MAXLINE, cli.fin) == NULL && feof(cli.fin)) {
                    errorlog("File EOF");
                    writen(cli.socksock, "exit\n", 5);
                }
                else {
                    writen(cli.socksock, cmdbuff, strlen(cmdbuff));
                    htmloutput(string(cmdbuff), cli.id, true);
                    errorlog("Command: " + string(cmdbuff));
                }
                cli.status = NOTWRITABLE;
            }
            if (FD_ISSET(cli.socksock, &rfds)) {
                char readbuf[MAXLINE];
                int len;
                if ((len = readn(cli.socksock, readbuf, MAXLINE)) < 0) {
                    errorlog("Read Error");
                }
                else if (len == 0) {
                    // FD_ISSET but read 0, connection closed by remote host
                    // TODO: FATAL
                    //
                    // FATAL
                    //
                    // if uncomment, will not work
                    // errorlog("client " + to_string(cli.id) + " exited");
                    // FD_CLR(cli.socksock, &afds);
                    // close(cli.socksock);
                    // FD_CLR(fileno(cli.fin), &afds);
                    // fclose(cli.fin);
                    // ++exit;
                    // cli.status = EXIT;
                    continue;
                }
                else {
                    readbuf[len] = '\0';
                    htmloutput(readbuf, cli.id);
                    if (string(readbuf).find("% ") != string::npos) {
                        cli.status = WRITABLE;
                    }
                }
            }
            // TODO: must sleep or not?
            // must
            // but if sleep too short, read not complete
            // need further work

            errorlog("sleep...");
            // usleep(80000);
        }
        usleep(80000);
    }
    return 0;
}

vector<pair<string, string> > parse_query(string query) {
    vector<pair<string, string> > parsed;
    int lpos = 0, rpos;
    string name, value;
    query += "&";
    while((rpos = query.find("=", lpos)) != string::npos) {
        name = query.substr(lpos, rpos-lpos);
        lpos = rpos + 1;
        rpos = query.find("&", lpos);
        value = query.substr(lpos, rpos-lpos);
        lpos = rpos + 1;
        parsed.push_back({name, value});
    }
    return parsed;
}

void html_init_output(vector<struct client> clients) {
    cout << "Content-Type: text/html\r\n\r\n";
    cout << "<html>\n \
             <head>\n \
             <meta http-equiv=\"Content-Type\" content=\"text/html; charset=big5\" />\n \
             <title>Network Programming Homework 3</title>\n \
             </head>\n \
             <body bgcolor=#336699>\n \
             <font face=\"Courier New\" size=2 color=#FFFF99>\n \
             <table width=\"800\" border=\"1\">\n \
             <tr>\n";
    for (auto cli:clients) {
        cout << "<td>" << cli.host << "</td>\n";
    }
    cout << "</tr>\n<tr>";
    for (int i = 0; i < clients.size(); ++i) {
        cout << "<td valign=\"top\" id=\"" << i << "\">";
        cout << "</td>\n";
    }
    cout << "</tr>\n";
    cout << "</table>\n";
    cout << "</font>\n</body>\n</html>";
}

void errorlog(string error_msg) {
    cout << "<script>console.log(`" << error_msg << "`);</script>" << endl;
}

void htmloutput(string msg, int id, bool is_command) {
    msg = replace_all(msg, "<", "&lt;");
    msg = replace_all(msg, ">", "&gt;");
    msg = replace_all(msg, "\n", "<br>");
    msg = replace_all(msg, "\r", "");
    msg = replace_all(msg, "\"", "&quot;");
    if (is_command) {
        msg = "<b>" + msg + "</b>";
    }
    // replace <, > by &lt, &gt
    cout << "<script>document.getElementById(\"" << id << "\")";
    cout << ".innerHTML += \"" << msg << "\";</script>";
    errorlog("htmloutput:" + to_string(id));
    errorlog(msg);
}

