#include <stdlib.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include <string>
#include <iostream>
#include <fstream>
#include <sys/stat.h>
#include <fcntl.h>
#include <sstream>

#include "utility.h"
#include "request_handler.h"

using namespace std;

RequestHandler::RequestHandler(int fd, struct sockaddr_in addr): sockfd(fd) {
    src_port = addr.sin_port;
    src_ip = inet_ntoa(addr.sin_addr);
}
RequestHandler::RequestHandler() {}
RequestHandler::~RequestHandler() {}

void RequestHandler::show_message() {
    cout << "<S_IP>\t:" << src_ip << endl;
    cout << "<S_PORT>\t:" << src_port << endl;
    cout << "<D_IP>\t:" << req.DST_IP << endl;;
    cout << "<D_PORT>\t:" << req.DST_PORT << endl;
}

bool RequestHandler::parse_request(unsigned char *buffer) {
    req.VN = buffer[0];
    req.CD = buffer[1];
    req.DST_PORT = buffer[2] << 8 |
               buffer[3];
    req.DST_IP = std::to_string(buffer[4]) + "."
           + std::to_string(buffer[5]) + "."
           + std::to_string(buffer[6]) + "."
           + std::to_string(buffer[7]);
    req.USER_ID = buffer + 8;
}

void RequestHandler::handle_request() {
    unsigned char readbuf[MAXLINE];
    // TODO: readn to unsigned char version
    read(sockfd, readbuf, MAXLINE);
    parse_request(readbuf);
    show_message();

    switch(req.CD) {
        case 1:
            cout << "<Command>\t:CONNECT" << endl;
            if (firewall_test(req.DST_IP, int(req.CD))) {
                cout << "<Reply>\t:Accept" << endl;
                // TODO: accept but connect fail??
                unsigned char package[8] = {0};
                package[1] = (unsigned char)90;
                send_reply(package);

                handle_connect();
            }
            else {
                cout << "<Reply>\t:Reject" << endl;
                unsigned char package[8] = {0};
                package[1] = (unsigned char)91;
                send_reply(package);
            }
            break;
        case 2:
            cout << "<Command>\t:BIND" << endl;
            if (firewall_test(req.DST_IP, int(req.CD))) {
                cout << "<Reply>\t:Accept" << endl;

                handle_bind();
            }
            else {
                cout << "<Reply>\t:Reject" << endl;
                unsigned char package[8] = {0};
                package[1] = (unsigned char)91;
                // TODO: some other settings
                send_reply(package);
            }
            break;
        default:
            cerr << "unknown CD: " << int(req.CD) << endl;
            break;
    }
}

void RequestHandler::handle_connect() {
    fd_set rfds, afds;
    int nfds = getdtablesize();
    FD_ZERO(&afds);

    int flags = fcntl(sockfd, F_GETFL, 0);
    fcntl(sockfd, F_SETFL, flags | O_NONBLOCK);
    FD_SET(sockfd, &afds);

    int sock;
    struct sockaddr_in sin;
    bzero((char*)&sin, sizeof(sin));
    sin.sin_family = AF_INET;
    sin.sin_port = htons((u_short)req.DST_PORT);
    sin.sin_addr.s_addr = inet_addr(req.DST_IP.c_str());
    sock = socket(PF_INET, SOCK_STREAM, 0);

    if (connect(sock, (struct sockaddr *) &sin, sizeof(sin)) < 0) {
        cerr << "Connect Failed" << endl;
        return;
    }

    flags = fcntl(sock, F_GETFL, 0);
    fcntl(sock, F_SETFL, flags | O_NONBLOCK);
    FD_SET(sock, &afds);

    int sockfds[2] = {sockfd, sock}; // socks4_server, client

    while(true) {
        memcpy(&rfds, &afds, sizeof(rfds));
        if (select(nfds, &rfds, (fd_set *)0, (fd_set *)0, (struct timeval *)0) < 0) {
            cerr << "Select Failed:" << strerror(errno) << endl;
        }

        for (int i = 0; i < 2; ++i) {
            if (FD_ISSET(sockfds[i], &rfds)) {
                unsigned char readbuf[MAXLINE];
                int len;
                if ((len = ureadn(sockfds[i], readbuf, MAXLINE)) < 0) {
                    cerr << "Read Error" << endl;
                }
                else if (len == 0) {
                    FD_CLR(sockfds[0], &afds);
                    close(sockfds[0]);

                    FD_CLR(sockfds[1], &afds);
                    close(sockfds[1]);

                    return;
                }
                else {
                    cout << "<Content>\t:" << readbuf << endl;
                    uwriten(sockfds[(i+1)%2], readbuf, len);
                }
            }
        }
    }
}

void RequestHandler::handle_bind() {
    int bind_sockfd, dst_sockfd;
    unsigned dstlen;
    struct sockaddr_in dst_addr, bind_addr;

    int BIND_TCP_PORT = 9876;

    // Open a TCP socket
    if ((bind_sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        std::cerr << "server: can't open stream socket" << std::endl;
    }

    // Bind our local address so that client can send to us
    bzero((char *) &bind_addr, sizeof(bind_addr));
    bind_addr.sin_family = AF_INET;
    bind_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    bind_addr.sin_port = htons(BIND_TCP_PORT);

    while (bind(bind_sockfd, (struct sockaddr *) &bind_addr, sizeof(bind_addr)) < 0) {
        BIND_TCP_PORT++;
        bind_addr.sin_port = htons(BIND_TCP_PORT);
    }

    unsigned char package[8] = {0};
    package[1] = (unsigned char)90;
    package[2] = BIND_TCP_PORT >> 8;
    package[3] = BIND_TCP_PORT & 0xFF;
    send_reply(package);

    listen(bind_sockfd, 5);

    dstlen = sizeof(dst_addr);
    dst_sockfd = accept(bind_sockfd, (struct sockaddr *) &dst_addr, &dstlen);
    if (dst_sockfd < 0) {
        std::cerr << "server: accept error" << std::endl;
    }

    close(bind_sockfd);

    // TODO: should check if accepted ip matches the requested ip
    send_reply(package);

    fd_set rfds, afds;
    int nfds = getdtablesize();
    FD_ZERO(&afds);

    FD_SET(sockfd, &afds);
    FD_SET(dst_sockfd, &afds);

    int sockfds[2] = {sockfd, dst_sockfd}; // socks4_server, client

    while(true) {
        memcpy(&rfds, &afds, sizeof(rfds));
        if (select(nfds, &rfds, (fd_set *)0, (fd_set *)0, (struct timeval *)0) < 0) {
            cerr << "Select Failed:" << strerror(errno) << endl;
        }

        for (int i = 0; i < 2; ++i) {
            if (FD_ISSET(sockfds[i], &rfds)) {
                unsigned char readbuf[MAXLINE];
                int len;
                if ((len = ureadn(sockfds[i], readbuf, MAXLINE)) < 0) {
                    cerr << "Read Error" << endl;
                }
                else if (len == 0) {
                    FD_CLR(sockfds[0], &afds);
                    close(sockfds[0]);

                    FD_CLR(sockfds[1], &afds);
                    close(sockfds[1]);

                    return;
                }
                else {
                    cout << "<Content>\t:" << readbuf << endl;
                    uwriten(sockfds[(i+1)%2], readbuf, len);
                }
            }
        }
    }
}

void RequestHandler::send_reply(unsigned char *package) {
    write(sockfd, package, 8);
}

bool RequestHandler::firewall_test(string ip, int mode) {
    fstream conf("socks.conf", fstream::in);
    string line;
    while(getline(conf, line)) {
        stringstream ss(line);
        string action, m, mask;
        ss >> action >> m >> mask;
        int mm;
        if (m == "c")
            mm = 1;
        else if (m == "b")
            mm = 2;
        if (mm != mode)
            continue;
        vector<string> ips = split(ip, ".");
        vector<string> masks = split(mask, ".");
        bool match = true;
        for (int i = 0; i < 4; ++i) {
            if (masks[i] != "*" && ips[i] != masks[i]) {
                cout << "mismatch: " << masks[i] << ' ' << ips[i] << endl;
                match = false;
                break;
            }
        }
        if (!match)
            continue;
        cout << "action == " << action << endl;
        if (action == "permit")
            return true;
        // deny
        // else
        //     return false;
        // TODO: general mask
    }
    // if not permit then deny
    return false;
}


