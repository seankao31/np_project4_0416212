#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include <string>
#include <unordered_map>

#include "utility.h"

class RequestHandler {
public:
    class Socks4Request {
    public:
        unsigned char VN;
        unsigned char CD;
        unsigned int DST_PORT;
        std::string DST_IP;
        unsigned char *USER_ID;

    public:
        Socks4Request() {}
        Socks4Request(unsigned char *buffer) {
            VN = buffer[0];
            CD = buffer[1];
            DST_PORT = buffer[2] << 8 |
                       buffer[3];
            DST_IP = std::to_string(buffer[4]) + "."
                   + std::to_string(buffer[5]) + "."
                   + std::to_string(buffer[6]) + "."
                   + std::to_string(buffer[7]);
            USER_ID = buffer + 8;
        }
        ~Socks4Request() {}
    };
public:
    int sockfd;
    std::string src_ip;
    int src_port;
    Socks4Request req;

public:
    RequestHandler(int fd, struct sockaddr_in addr);
    RequestHandler();
    ~RequestHandler();

    void handle_request();
    void show_message();
    bool parse_request(unsigned char *buffer);
    void handle_connect();
    void handle_bind();
    void send_reply(unsigned char *package);
    bool firewall_test(std::string ip, int mode);
};
