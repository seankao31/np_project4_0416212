CGI_OBJ = hw4_cgi.o utility_bsd.o
SOCKS4_OBJ = socks4_server.o request_handler.o utility.o
DEPS = request_handler.h utility.h
FLAGS = -std=c++11

.PHONY: sock cgi

all: socks4_server

sock: socks4_server

# should use bsd
cgi: hw4.cgi
	rm -f ~/public_html/hw4.cgi
	rm -f ~/public_html/.nfs.*
	cp hw4.cgi ~/public_html/
	rm -f ~/np_pj3/myhttpserver/hw4.cgi
	rm -f ~/np_pj3/myhttpserver/.nfs.*
	cp hw4.cgi ~/np_pj3/myhttpserver/

socks4_server: $(SOCKS4_OBJ)
	g++ -o $@ $+ $(FLAGS)

hw4.cgi: $(CGI_OBJ)
	g++ -o $@ $+ $(FLAGS) -static

utility_bsd.o: utility.cpp $(DEPS)
	g++ -c -o $@ $< $(FLAGS)

%.o: %.cpp $(DEPS)
	g++ -c -o $@ $< $(FLAGS)

clean:
	rm -f socks4_server hw4.cgi *.o
